== Build test images ==

The test images are based on docker-obs's minbase, obs/obs-api,
obs/obs-server and obs/worker images. If you haven't create them,
you will probably a `pull access denied` for these images.
Please follow the README.md under docker-obs project root dir to
create these images first.

To build the test images and launch in multiple containers with compose,
execute following commands under `docker-obs/obs-tests` directory:

```
 $ sudo make
```
== Run containers ==

1. Run the containers
```
 $ sudo docker-compose up
```

== Apply the latest built OBS binaries ==

For test latest OBS that build from your ccu OBS homedir.
You may use `osc getbinaries PROJECT PACKAGE REPOSITORY ARCHITECTURE`
to fetch binaries under /tmp:
eg:
1. Switch to /tmp
```
 $ cd /tmp
```
2. Fetch binaries from your OBS homedir
```
 $ osc getbinaries home:YOUR:stretch open-build-service Debian_Stretch_backports_main x86_64
```
3. Execute script to update the latest OBS binares fetched under /tmp inside the containers
```
 $ cd -
 $ sudo ./update-obs-binaries.sh
```

== Access Open Build Service UI ==

1. Open Build Service should be now running, point browser to
   `https://localhost/`

== Access to the shell of the containers ==

The worker container:
```
 docker exec -it obstests_worker_1 /bin/bash
```
The obs-api container:
```
 docker exec -it obstests_obs-api_1 /bin/bash
```
The obs-server container:
```
 docker exec -it obstests_obs-server_1 /bin/bash
```
OBS logs are localted under /var/log/obs/ inside the containers.

== test_dod_1.sh ==

The test_dod_1.sh is designed to test DoD stup and build a hello package
from debian.

You may test it by add following line into your /etc/hosts
```
 127.0.0.1	obs-api
```
and then run
```
 test_dod_1.sh obs-api
```
The script will setup debian projects and DoD automaticlly and submit
`hello` package build. You may check the webUI again once you ran the
script.

== Stop the containers ==
Press Ctrl-C in the console that runs the containers.

== Restart the tests from scratch ==
Stop the containers first. And then:
```
 $ sudo make clean
 $ sudo make
```
