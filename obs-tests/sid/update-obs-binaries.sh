#!/bin/bash
OPTIONS="-o DPkg::Options::=--force-confdef -y --no-install-recommends --allow-downgrades"
BINPATH="/tmp/binaries"

docker exec -i sid_worker_1 /bin/bash -c \
	"apt install $OPTIONS $BINPATH/obs-worker*.deb"

docker exec -i sid_obs-server_1 /bin/bash -c \
	"apt install $OPTIONS $BINPATH/obs-server*.deb \
			      $BINPATH/obs-productconverter*.deb"

docker exec -i sid_obs-api_1 /bin/bash -c \
	"apt install $OPTIONS $BINPATH/obs-api*.deb"

# Post install for new obs-api package
docker exec -i sid_obs-api_1 /bin/bash -c \
	"/usr/share/obs/api/script/rake-tasks.sh setup"
docker exec -i sid_obs-api_1 /bin/bash -c \
	"service apache2 restart"
